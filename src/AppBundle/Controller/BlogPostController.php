<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogPost;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Blogpost controller.
 *
 */
class BlogPostController extends Controller
{
    /**
     * Lists all blogPost entities.
     * @Route("/blog/index", name="blogpost_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        ////////////////////////////////////
        // FUNCIONA ASSIM MAS SEM PARAMETROS

        /*
        $sql = " 
        SELECT id as 'titulo', title, content, created_at
          FROM blog_post
        ";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        
        $blogPosts = $stmt->fetchAll();
        */



        //////////////////////////////////////////////////////////
        // FUNCIONA ASSIM MAS COM PARAMETROS, MAS NAO FUNCIONA A %
        /*
        $em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT title AS 'titulo original' FROM blog_post WHERE title = :title");
        $statement->bindValue('title', 'tit%');
        $statement->execute();
        $blogPosts = $statement->fetchAll();
        */


        /////////////////////////////////////////////////
        // FUNCIONA MAS NAO POSSO METER ALIAS COM ESPACOS
        /*
        $blogPosts = $em->getRepository("AppBundle:BlogPost")->createQueryBuilder('o')
        ->select('o.title as sopas', 'o.content as sopas1')
        ->andWhere('o.title LIKE :product')
        ->andWhere('o.content LIKE :content')
        ->setParameter('product', '%')
        ->setParameter('content', '%')
        ->getQuery()
        ->getResult();
        */

        // para mudar o nome das keys, tem de ser todas
        /*
        $blogPosts = array_map(function($tag) {
            return array(
                'value' => $tag['sopas']
            );
        }, $blogPosts);
        */

        // $blogPosts[0]['sopas'] = $blogPosts[0]['sopas qwe'];

        // NORMAL QUERY
        //$blogPosts = $em->getRepository('AppBundle:BlogPost')->findAll();
        //$blogComments = $em->getRepository('AppBundle:BlogComment')->findAll();

        $qb = $em->createQueryBuilder();
        $qb->select('u.author', 'u.content', 'u.email', 'p.title', 'u.id as idComment', 'p.id as idPost')
            ->from('AppBundle:BlogComment', 'u')
            ->innerJoin('AppBundle:BlogPost', 'p', 'WITH', 'u.post = p.id');
            //->leftJoin('u.post', 'p');

        // Example - $qb->innerJoin('u.Group', 'g', Expr\Join::WITH, $qb->expr()->eq('u.status_id', '?1'))
        // Example - $qb->innerJoin('u.Group', 'g', 'WITH', 'u.status = ?1')
        // Example - $qb->innerJoin('u.Group', 'g', 'WITH', 'u.status = ?1', 'g.id')

        $query = $qb->getQuery();

        print_r($qb->getDQL());

        echo "<pre>";
        print_r($query->getResult());
        echo "</pre>";

        die;

        return $this->render('blogpost/index.html.twig', array(
            'blogPosts' => $blogPosts,
            'blogComments' => $blogComments,
        ));
    }

    /**
     * Creates a new blogPost entity.
     * @Route("/blog/new", name="blogpost_new")
     */
    public function newAction(Request $request)
    {
        $blogPost = new Blogpost();
        $form = $this->createForm('AppBundle\Form\BlogPostType', $blogPost);
        $form->handleRequest($request);

        $logger = $this->get('logger');
        $logger->info('new action is being created');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blogPost);
            $em->flush();

            $logger->info('new blog post created. title: ' . $blogPost->getTitle() . ' | content: ' . $blogPost->getContent());

            return $this->redirectToRoute('blogpost_show', array('id' => $blogPost->getId()));
        }

        return $this->render('blogpost/new.html.twig', array(
            'blogPost' => $blogPost,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a blogPost entity.
     * @Route("/blog/show/{id}", name="blogpost_show")
     */
    public function showAction(BlogPost $blogPost)
    {
        $deleteForm = $this->createDeleteForm($blogPost);

        $em = $this->getDoctrine()->getManager();

        //query to get comments from post showed
        $blogComments = $em->getRepository('AppBundle:BlogComment')->findBy( array('post' => $blogPost->getId()), array('id' => 'DESC') );

        return $this->render('blogpost/show.html.twig', array(
            'blogPost' => $blogPost,
            'delete_form' => $deleteForm->createView(),
            'blogComments' => $blogComments
        ));
    }

    /**
     * Displays a form to edit an existing blogPost entity.
     * @Route("/blog/edit/{id}", name="blogpost_edit")
     */
    public function editAction(Request $request, BlogPost $blogPost)
    {
        $deleteForm = $this->createDeleteForm($blogPost);
        $editForm = $this->createForm('AppBundle\Form\BlogPostType', $blogPost);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('blogpost_edit', array('id' => $blogPost->getId()));
        }

        return $this->render('blogpost/edit.html.twig', array(
            'blogPost' => $blogPost,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a blogPost entity.
     * @Route("/blog/delete/id={id}", name="blogpost_delete")
     */
    public function deleteAction(Request $request, BlogPost $blogPost)
    {
        $form = $this->createDeleteForm($blogPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($blogPost);
            $em->flush();
        }

        return $this->redirectToRoute('blogpost_index');
    }

    /**
     * Creates a form to delete a blogPost entity.
     *
     * @param BlogPost $blogPost The blogPost entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BlogPost $blogPost)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blogpost_delete', array('id' => $blogPost->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
