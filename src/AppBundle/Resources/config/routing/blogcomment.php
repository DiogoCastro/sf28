<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('blogcomment_index', new Route(
    '/',
    array('_controller' => 'AppBundle:BlogComment:index'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('blogcomment_show', new Route(
    '/{id}/show',
    array('_controller' => 'AppBundle:BlogComment:show'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('blogcomment_new', new Route(
    '/new',
    array('_controller' => 'AppBundle:BlogComment:new'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('blogcomment_edit', new Route(
    '/{id}/edit',
    array('_controller' => 'AppBundle:BlogComment:edit'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('blogcomment_delete', new Route(
    '/{id}/delete',
    array('_controller' => 'AppBundle:BlogComment:delete'),
    array(),
    array(),
    '',
    array(),
    array('DELETE')
));

return $collection;
